# {{profile retrie question}}

### HTTP Request
```
{{POST /profile/retrieQuestion}}
```
### Example
##### Request

```
        [{
            "question_no": "1",
            "answer_text":"200,000"
            
        },{
            "question_no": "2",
            "answer_text":"20"
            
        },{
            "question_no": "3",
            "answer_text":"50"	
        }]

```

##### Response

```
{
    "message": "success"
}
```


==========================================================

### HTTP Request
```
{{GET /profile/retrieQuestion}}
```
### Example
##### Request


##### Response

```
    [{
            "question_no": "1",
            "answer_text":"200,000"
            
        },{
            "question_no": "2",
            "answer_text":"20"
            
        },{
            "question_no": "3",
            "answer_text":"50"	
        }]
```

