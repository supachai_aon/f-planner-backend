const Question = require('../models/question.model')
const initData = require('../service/initQuestionData')


exports.findAll = (req, res, next) => {
    Question.find({"question_type":req.params.qType}, { _id : 0,"answers._id":0 }).exec(function (err, results) {
        if (err) { return next(err) }   
        res.json(results) 
    })
}

exports.findByQuestionNo = (req, res, next) => {
    Question.find({"question_no":req.params.qNo,"question_type":req.params.qType}, { _id : 0,"answers._id":0 }).exec(function (err, results) {
        if (err) { return next(err) }  
        res.json(results[0]) 
    }) 
}







//* for  init
exports.init = (req, res, next) => {
    for(q of initData){
        create(q)
    }
    res.json("done")       
}

const create = (q) => {
    const question = new Question({
            question_type: q.question_type,
            question_no: q.question_no,   
            question_name: q.question_name,
            answers : q.answers
    })
    question.save(err => { 
        if (err) {
            console.log(err)
        }     
    })   
}
