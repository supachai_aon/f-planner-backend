# {{questions list}}

{{list of question in db}}


### HTTP Request

```
{{GET /questions/:qType/:qNo}}
```
### Request parameters

qType   =  'common'  or  'retrie'
qNo  =  number of question    (if have not it will show all question in any qType)

### Example

##### Request

```http  GET /questions/common/1
```

##### Response

``` {
    "question_type": "common",
    "question_no": 1,
    "question_name": "ปัจจุบันท่านมีภาระค่าใช้จ่ายรายเดือน รวมเป็นสัดส่วนเท่าใดของรายได้ต่อเดือน",
    "__v": 0,
    "answers": [
        {
            "answer_no": 1,
            "answer_name": "มากกว่า 80%",
            "answer_point": 1
        },
        {
            "answer_no": 2,
            "answer_name": "มากกว่า 50% ถึง 80%",
            "answer_point": 2
        },
        {
            "answer_no": 3,
            "answer_name": "มากกว่า 20% ถึง 50%",
            "answer_point": 3
        },
        {
            "answer_no": 4,
            "answer_name": "ไม่เกิน 20%",
            "answer_point": 4
        }
    ]
}
```

======================================

##### Request

```http  GET /questions/retrie
```

##### Response

``` [
    {
        "question_type": "retrie",
        "question_no": 1,
        "question_name": "หยุดทำงานเมื่อไหร",
        "__v": 0,
        "answers": []
    },
    {
        "question_type": "retrie",
        "question_no": 3,
        "question_name": "ออมเพิ่มต่อเดือน",
        "__v": 0,
        "answers": []
    },
    {
        "question_type": "retrie",
        "question_no": 2,
        "question_name": "เงินลงทุนเริ่มต้น",
        "__v": 0,
        "answers": []
    }

    
]
```
