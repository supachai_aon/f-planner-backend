const userService = require('../service/user_service')
const firebase = userService.firebase;
const admin = userService.admin;
const getToken = userService.getToken;
const isAuthenticated = userService.isAuthenticated;

exports.signup = (req, res, next) => {
        const newUserEmail = req.body.email;
        const newUserPass = req.body.password;

       firebase.auth().createUserWithEmailAndPassword
        (
            newUserEmail,
            newUserPass
        )
        .then(function(userRecord)
        {
            
            return getToken(newUserEmail,newUserPass,res)

        })
        .catch(function(error)
        {
             return res.status(401).send(error.message);
        });

}

exports.login = (req, res, next) => {

    const userEmail = req.body.email;
    const userPassword = req.body.password;
    
    return getToken(userEmail,userPassword,res)
}
