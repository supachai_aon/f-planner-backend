module.exports  = [
    {
        "question_type":'common',
        question_no: 1,
        question_name: "ปัจจุบันท่านมีภาระค่าใช้จ่ายรายเดือน รวมเป็นสัดส่วนเท่าใดของรายได้ต่อเดือน",
        answers : [{
            "answer_no": 1,   
            "answer_name": "มากกว่า 80%",
            "answer_point": 1
        },
        {
            "answer_no": 2,   
            "answer_name": "มากกว่า 50% ถึง 80%",
            "answer_point": 2
        },
        {
            "answer_no": 3,   
            "answer_name": "มากกว่า 20% ถึง 50%",
            "answer_point": 3

        },
        {
            "answer_no": 4,   
            "answer_name": "ไม่เกิน 20%",
            "answer_point": 4

        }]
    }
    ,
    {
        "question_type":'common',
        "question_no": 2,
        "question_name": "หากท่านออกจากงานวันนี้เงินออมที่ท่านมีอยู่จะรองรับค่าใช้จ่ายได้นานแค่ไหน",
        "answers" : [{
            "answer_no": 1,   
            "answer_name": "น้อยกว่า 3 เดือน",
            "answer_point": 1
        },
        {
            "answer_no": 2,   
            "answer_name": "3 เดือน ถึง 1 ปี",
            "answer_point": 2
        },
        {
            "answer_no": 3,   
            "answer_name": "มากกว่า 1 ปีถึง 3 ปี",
            "answer_point": 3

        },
        {
            "answer_no": 4,   
            "answer_name": "มากกว่า 3 ปี",
            "answer_point": 4

        }]
    },
    {
        "question_type":'common',
        "question_no": 3,
        "question_name": "จำนวนเงินกองทุนสำรองเลี้ยงชีพ ณ ปัจจุบัน คิดเป็นสัดส่วนเท่าใดของทรัพย์สินทั้งสิ้นของท่าน",
        "answers" : [{
            "answer_no": 1,   
            "answer_name": "มากกว่า 75%",
            "answer_point": 1
        },
        {
            "answer_no": 2,   
            "answer_name": "มากกว่า 50% ถึง75%",
            "answer_point": 2
        },
        {
            "answer_no": 3,   
            "answer_name": "มากกว่า 25% ถึง50%",
            "answer_point": 3

        },
        {
            "answer_no": 4,   
            "answer_name": "ไม่เกิน 25%",
            "answer_point": 4

        }]
    },
    {
        "question_type":'common',
        "question_no": 4,
        "question_name": "หากท่านต้องเข้ารักษาตัวในโรงพยาบาลหลังจากเกษียณอายุ แล้วท่านจะหาเงินค่ารักษาพยาบาลจากไหน",
        "answers" : [{
            "answer_no": 1,   
            "answer_name": "เงินออมของตนเอง",
            "answer_point": 1
        },
        {
            "answer_no": 2,   
            "answer_name": "ให้ครอบครัวร่วมรับผิดชอบ ",
            "answer_point": 2
        },
        {
            "answer_no": 3,   
            "answer_name":  "สวัสดิการต่างๆ",
            "answer_point": 3

        },
        {
            "answer_no": 4,   
            "answer_name": "ประกัน สุขภาพ",
            "answer_point": 4

        }]
    },
    {
        "question_type":'common',
        "question_no": 5,
        "question_name": "ท่านรู้จักการลงทุนอะไรบ้าง",
        "answers" : [{
            "answer_no": 1,   
            "answer_name": "เงินฝาก",
            "answer_point": 1
        },
        {
            "answer_no": 2,   
            "answer_name": "เงินฝาก พันธบัตร หุ้นกู้  ",
            "answer_point": 2
        },
        {
            "answer_no": 3,   
            "answer_name": "เงินฝาก พันธบัตร หุ้นกู้กองทุนรวม หุ้นสามัญ ",
            "answer_point": 3

        },
        {
            "answer_no": 4,   
            "answer_name": "เงินฝาก พันธบตัร หุ้นกู้กองทุนรวม หุ้นสามัญกองทุนรวมทองคำ กองทุนรวมอสังหาริมทรัพย์และอื่นๆ ",
            "answer_point": 4

        }]
    },
    {
        "question_type":'common',
        "question_no": 6,
        "question_name": "ท่านมีประสบการณ์การลงทุน อะไรบ้าง",
        "answers" : [{
            "answer_no": 1,   
            "answer_name": "เงินฝาก",
            "answer_point": 1
        },
        {
            "answer_no": 2,   
            "answer_name": "เงินฝาก พันธบัตร หุ้นกู้  ",
            "answer_point": 2
        },
        {
            "answer_no": 3,   
            "answer_name": "เงินฝาก พันธบัตร หุ้นกู้กองทุนรวม หุ้นสามัญ ",
            "answer_point": 3

        },
        {
            "answer_no": 4,   
            "answer_name": "เงินฝาก พันธบตัร หุ้นกู้กองทุนรวม หุ้นสามัญกองทุนรวมทองคำ กองทุนรวมอสังหาริมทรัพย์และอื่นๆ ",
            "answer_point": 4

        }]
    },
    {
        question_type:'common',
        "question_no": 7,
        "question_name": "ทัศนคติในการลงทุนของท่าน",
        "answers" : [{
            "answer_no": 1,   
            "answer_name": "ไม่สามารถทนต่อการขาดทุนเงินต้นได้เลย",
            "answer_point": 1
        },
        {
            "answer_no": 2,   
            "answer_name": "สามารถทนต่อการขาดทุนเงินต้นได้บ้างเพื่อมีโอกาสได้รับผลตอบแทนที่สูงข้ึน   ",
            "answer_point": 2
        },
        {
            "answer_no": 3,   
            "answer_name": "สามารถทนต่อการขาดทุนเงินต้นได้มาก เพื่อมีโอกาสได้รับผลตอบแทนสูง ",
            "answer_point": 3

        },
        {
            "answer_no": 4,   
            "answer_name": "อยากได้ผลตอบแทนสูงสุด โดยไม่มีข้อจำกัดในการลงทุน ",
            "answer_point": 4

        }]
    },
    {
        "question_type":'common',
        "question_no": 8,
        "question_name": "เป้าหมายการลงทุนของท่านเป็นอย่างไร",
        "answers" : [{
            "answer_no": 1,   
            "answer_name": "เงินต้นต้องปลอดภัยแม้ว่า จะได้รับผลตอบแทนตำ่ กว่าอัตราเงินเฟ้อ",
            "answer_point": 1
        },
        {
            "answer_no": 2,   
            "answer_name": "ต้องการผลตอบแทนสูงกว่าอัตราเงินเฟ้อโดยสามารถรับความผันผวนของมูลค่าเงินกองทุนไดบ้าง ",
            "answer_point": 2
        },
        {
            "answer_no": 3,   
            "answer_name": "ต้องการผลตอบแทนสูงกว่าอัตราเงินเฟ้อมากโดยสามารถรับความผันผวนของมูลค่าเงินกองทุนได้มาก",
            "answer_point": 3

        },
        {
            "answer_no": 4,   
            "answer_name":"ต้องการผลตอบแทนสูงกว่าอัตราเงินเฟ้อมากที่สุดโดยสามารถรับความผันผวนของมูลค่าเงินกองทุนได้เต็มที่",
            "answer_point": 4

        }]
    },

    {
        "question_type":'common',
        "question_no": 9,
        "question_name": "ท่านคาดหวังผลตอบแทนจากการลงทุนในกองทุนสำรองเลี้ยงชีพในระดับใดต่อปี",
        "answers" : [{
            "answer_no": 1,   
            "answer_name": "ประมาณ 2% ถึง3% อย่างสม่าเสมอ",
            "answer_point": 1
        },
        {
            "answer_no": 2,   
            "answer_name": "มีโอกาสได้รับผลตอบแทนถึง 5% แต่บางปีอาจไม่มีผลตอบแทนเลย",
            "answer_point": 2
        },
        {
            "answer_no": 3,   
            "answer_name": "มีโอกาสได้รับผลตอบแทนถึง 8% แต่บางปีอาจขาดทุนได้ถึง 3% ",
            "answer_point": 3

        },
        {
            "answer_no": 4,   
            "answer_name":"มีโอกาสได้รับผลตอบแทนถึง 25%  แต่บางปีอาจขาดทุนได้ถึง 15%",
            "answer_point": 4

        }]
     },
     {
        "question_type":'retrie',
        "question_no": 1,
        "question_name": "หยุดทำงานเมื่อไหร",
     },
     {
        "question_type":'retrie',
        "question_no": 2,
        "question_name": "เงินลงทุนเริ่มต้น",
     },
     {
        "question_type":'retrie',
        "question_no": 3,
        "question_name": "ออมเพิ่มต่อเดือน",
     }
]