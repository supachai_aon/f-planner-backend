const mongoose = require('mongoose')    
const Schema = mongoose.Schema


const profileSchema = new Schema({
    user_id: {type :String, unique: true, index: true },
    common_question : [
        {
            question_no: Number,
            answer_no: Number
        }
    ],
    retrie_question : [
        {
            question_no: Number,
            answer_text: String
        }
    ],
    personal_info: {
        title_name : Number,
        firstname_thai:String,
        lastname_thai:String,
        firstname_eng:String,
        lastname_eng:String,
        id_card : Number,
        issue_by:String,
        issued_date:String,
        expire_date:String,
        gender:Number,
        birthday:String,
        marital_status:Number,
        number_of_children:Number,
        email:String,
        mobile:String,
        education:Number,
        occupation:String,
        business_type:String,
        position:String,
        regular_income:Number,
        other_income:Number,
        other_incom_from:String
    }
    
})



const ModelClass = mongoose.model('profile', profileSchema)

module.exports = ModelClass