const firebase = require('firebase');
const admin = require('firebase-admin');
const config = require('../config') 

firebase.initializeApp(config.firebase_config);
admin.initializeApp({
  credential: admin.credential.cert(config.firebase_admin_config),
  databaseURL: config.firebase_databaseURL
});


const isAuthenticated = (req,res,next)=>{
    const idToken = req.headers['authorization']

    if(!idToken){
        return res.status(401).send('Unauthorized'); 
    }
    admin.auth().verifyIdToken(idToken)
        .then(function(decodedToken) {

            req.params.user_id = decodedToken.user_id
            console.log(req.params.user_id)
            next();

        }).catch(function(error) {
            
            return res.status(401).send('Unauthorized');
        });
}


const getToken = (userEmail,userPassword,res)=>{

    firebase.auth().signInWithEmailAndPassword(userEmail, userPassword)
    .then(function(firebaseUser) {
        
        firebaseUser.getIdToken().then(function(token){
          return res.json({
           status: 200, 
            token: token
         });
        });
         
    })
    .catch(function(error) {
        return res.status(401).send('Unauthorized');
    });
}

module.exports = {
    firebase,
    admin,
    isAuthenticated,
    getToken
 }


