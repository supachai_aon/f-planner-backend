# {{profile personal info}}

### HTTP Request
```
{{POST /profile/info}}
```
### Example
##### Request

```
    {
                title_name : Number,  // คำนำหน้า  1 = นาย  2 = นางสาว  3 = นาง
                firstname_thai:String, // ชื่อไทย
                lastname_thai:String, // นามสกุลไทย
                firstname_eng:String, // ชื่อ eng
                lastname_eng:String, //  ชนามสกุล eng
                id_card : Number, //  เลขประชาชน
                issue_by:String, //  ออกโดย
                issued_date:String, // วันที่ออกบัตร  วัน/เดือน/ปี 
                expire_date:String, // วันที่บัตรหมดอายุ  วัน/เดือน/ปี 
                gender:Number, // เพศ    1=ชาย   2= หญิง
                birthday:String,  // วันเกิด  วัน/เดือน/ปี 
                marital_status:Number,  // สถานะภาพสมรส   1=โสด  2= สมรส  3= หย่า  4=หม้าย
                number_of_children:Number, // จำนวนลูก
                email:String, 
                mobile:String,
                education:Number,  // ระดับการศึกษา   1= ประถม  2=มัธยม 3=ปวช 4=ปวส/ปอนุริญญา  5=ปริญญาตรี      6=ปริญญาโท   7=ปริญญาเอก
                
                occupation:String,   //อาชีพ  
                business_type:String,  // ประเภทธุรกิจ
                position:String,  // ตำแหน่ง
                regular_income:Number,  // รายได้ประจำ
                other_income:Number,  //  รายได้อื่นๆ
                other_incom_from:String // ที่มาของรายได้อื่นๆ
    }

```

##### Response

```
{
    "message": "success"
}
```


==========================================================

### HTTP Request
```
{{GET /profile/info}}
```
### Example
##### Request


##### Response

```
{
                title_name : Number,  // คำนำหน้า  1 = นาย  2 = นางสาว  3 = นาง
                firstname_thai:String, // ชื่อไทย
                lastname_thai:String, // นามสกุลไทย
                firstname_eng:String, // ชื่อ eng
                lastname_eng:String, //  ชนามสกุล eng
                id_card : Number, //  เลขประชาชน
                issue_by:String, //  ออกโดย
                issued_date:String, // วันที่ออกบัตร  วัน/เดือน/ปี 
                expire_date:String, // วันที่บัตรหมดอายุ  วัน/เดือน/ปี 
                gender:Number, // เพศ    1=ชาย   2= หญิง
                birthday:String,  // วันเกิด  วัน/เดือน/ปี 
                marital_status:Number,  // สถานะภาพสมรส   1=โสด  2= สมรส  3= หย่า  4=หม้าย
                number_of_children:Number, // จำนวนลูก
                email:String, 
                mobile:String,
                education:Number,  // ระดับการศึกษา   1= ประถม  2=มัธยม 3=ปวช 4=ปวส/ปอนุริญญา  5=ปริญญาตรี      6=ปริญญาโท   7=ปริญญาเอก
                
                occupation:String,   //อาชีพ  
                business_type:String,  // ประเภทธุรกิจ
                position:String,  // ตำแหน่ง
                regular_income:Number,  // รายได้ประจำ
                other_income:Number,  //  รายได้อื่นๆ
                other_incom_from:String // ที่มาของรายได้อื่นๆ
    }
```

