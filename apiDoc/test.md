# {{questions list}}

{{list of question in db}}


### HTTP Request

```
{{GET /questions/:qType/:qNo}}
```
### Request parameters

In the request URL, provide the following query parameters with values.

| Parameter | Type | Description |
|:----------|:-----|:------------|

### Request body

Do not supply a request body with this method.

### Example

##### Request

```http
```

##### Response

```http
```
