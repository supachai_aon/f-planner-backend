# {{profile common question}}

### HTTP Request
```
{{POST /profile/commonQuestion}}
```
### Example
##### Request

```
        [{
            "question_no": "1",
            "answer_no":"2"
            
        },{
            "question_no": "2",
            "answer_no":"1"
            
        },{
            "question_no": "3",
            "answer_no":"3"	
        }]

```

##### Response

```
{
    "message": "success"
}
```


==========================================================

### HTTP Request
```
{{GET /profile/commonQuestion}}
```
### Example
##### Request


##### Response

```
[{
            "question_no": "1",
            "answer_no":"2"
            
        },{
            "question_no": "2",
            "answer_no":"1"
            
        },{
            "question_no": "3",
            "answer_no":"3"	
        }]
```

