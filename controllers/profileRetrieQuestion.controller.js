const Profile = require('../models/profile.model')




function findDuplicates(data) {

    let result = [];
  
    data.forEach(function(element, index) {
      
      if (data.indexOf(element, index + 1) > -1) {
        
        if (result.indexOf(element) === -1) {
          result.push(element);
        }
      }
    });
  
    return result;
  }

exports.save = (req, res, next) => {

    const questions = req.body
    console.log(questions)

    if (typeof questions !== 'undefined' && questions.length > 0) {
        
    }else {
        return res.json("error") 
    }

    const isDup= findDuplicates(questions.map(x=>x.question_no))

    if(isDup.length > 0){
        return res.json("error") 
    }

    const profile = {
        user_id: req.params.user_id,
        retrie_question : questions
    }



    Profile.findOneAndUpdate(
        {user_id: req.params.user_id},
        profile,
        {upsert: true, new: true, runValidators: true},
        function (err, doc) {
            if (err) {
                return res.status(400).json({ message: err })
            } else {
              
                return res.status(200).json({message : "success"})
            }
        }
    );
}


exports.findByUserId = (req, res, next) => {
    Profile.findOne({"user_id":req.params.user_id}, { _id : 0,"user_id":0,"retrie_question._id":0 }).exec(function (err, results) {
        if (err) { return next(err) }  

        if(results)
         "retrie_question" in results ? res.json(results.retrie_question)  : res.json(results)

        else {
            res.json({})   
        }
  
        //res.json(results.common_question) 
    }) 
}