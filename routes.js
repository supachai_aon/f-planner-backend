
const question = require('./controllers/question.controller')
const user = require('./controllers/user.controller')
const profileCommonQuestion = require('./controllers/profileCommonQuestion.controller')
const profileRetrieQuestion = require('./controllers/profileRetrieQuestion.controller')

const profilePersonalInfo = require('./controllers/profilePersonalInfo.controller')

const userService = require('./service/user_service')
const firebase = userService.firebase;
const admin = userService.admin;

const getToken = userService.getToken;
const isAuthenticated = userService.isAuthenticated;



module.exports = function (app) {
    app.get('/', function (req, res) {
        res.send({ message: 'itService MongoDB2' })
    })

    
    app.get('/questions/:qType',isAuthenticated, question.findAll)
    app.get('/questions/init',isAuthenticated, question.init)
    app.get('/questions/:qType/:qNo',isAuthenticated, question.findByQuestionNo)

    app.post('/profile/commonQuestion',isAuthenticated, profileCommonQuestion.save)
    app.get('/profile/commonQuestion',isAuthenticated, profileCommonQuestion.findByUserId)

    app.post('/profile/retrieQuestion',isAuthenticated, profileRetrieQuestion.save)
    app.get('/profile/retrieQuestion',isAuthenticated, profileRetrieQuestion.findByUserId)


    app.post('/profile/info',isAuthenticated, profilePersonalInfo.save)
    app.get('/profile/info',isAuthenticated, profilePersonalInfo.findByUserId)
   

    

    
    app.post('/signup',user.signup);
    app.post('/login', user.login);


}

