const Profile = require('../models/profile.model')


exports.save = (req, res, next) => {

    const personalInfo = req.body
    

    const profile = {
        user_id: req.params.user_id,
        personal_info : personalInfo
    }



    Profile.findOneAndUpdate(
        {user_id: req.params.user_id},
        profile,
        {upsert: true, new: true, runValidators: true},
        function (err, doc) {
            if (err) {
                return res.status(400).json({ message: err })
            } else {
              
                return res.status(200).json({message : "success"})
            }
        }
    );
}


exports.findByUserId = (req, res, next) => {
    Profile.findOne({"user_id":req.params.user_id}, { _id : 0,"user_id":0,"personal_info._id":0 }).exec(function (err, results) {
        if (err) { return next(err) }  

        if(results)
            "personal_info" in results ? res.json(results.personal_info)  : res.json(results)

        else {
            res.json({})   
        }
  
        //res.json(results.common_question) 
    }) 
}
