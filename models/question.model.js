const mongoose = require('mongoose')    
const Schema = mongoose.Schema

// const questionSchema = new Schema({
//     question_no: {type :Number, unique: true, index: true },
//     question_name: String,
//     answers : [
//         {
//             answer_no: Number,   
//             answer_name: String,
//             answer_point: Number, 
//         }
//     ]
// })


const questionSchema = new Schema({
        question_no: {type :Number },
        question_name: String,
        question_type:{type :String,enum: ['common', 'retrie'] },
        answers : [
            {
                answer_no: Number,   
                answer_name: String,
                answer_point: Number, 
            }
        ]
    })


const ModelClass = mongoose.model('question', questionSchema)


module.exports = ModelClass


let enumType = {}
enumType["common"] = "คำถามทั่วไป";
enumType["retrie"] = "คำถามเกษียณ";
